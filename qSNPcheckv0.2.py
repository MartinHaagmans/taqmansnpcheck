import logging
from pathlib import Path

logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s', 
    filename='taqmanParse.log',
    level=logging.INFO
    )

def run_command(cmd):
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError:
        logging.error(f"ಠ_ಠ Error in command: {cmd}")
    else:
        logging.info(f"ʘ‿ʘ Command {cmd} executed successfully")
    return

NSXDIR = Path("/mnt/TargetedPanels/Targeted Panels/SNPcheck/")

class qSNPcheck:
    
    def __init__(self, parsed_ixo_files, fn_platelayout):
        self.parsed_ixo_files = parsed_ixo_files
        fn_platelayout_csv = fn_platelayout.replace('.xls', '.csv')
        pd.read_excel(fn_platelayout).to_csv(fn_platelayout_csv, index=False)
        self.sample_position = self._apply_conversion(fn_platelayout_csv)
        self.df = self._get_qpcr_results_dataframe()

    def _get_conversion(self):
        conversion = dict()
        NUMBERS = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11']
        LETTERS = {
            'A': ('A', 'B'),
            'B': ('C', 'D'),
            'C': ('E', 'F'),
            'D': ('G', 'H'),
            'E': ('I', 'J'),
            'F': ('K', 'L'),
            'G': ('M', 'N'),
            'H': ('O', 'P')
        }

        for letter in LETTERS:
            count = 1 
            for number in NUMBERS:
                if count < 10:
                    out1 = f'0{count}'
                elif count >= 10:
                    out1 = f'{count}'
                if count + 1 < 10:
                    out2 = f'0{count+1}'
                elif count + 1 >= 10:
                    out2 = f'{count+1}'

                conversion[f'{letter}{number}'] = (
                    f'{LETTERS[letter][0]}{out1}', 
                    f'{LETTERS[letter][0]}{out2}', 
                    f'{LETTERS[letter][1]}{out1}', 
                    f'{LETTERS[letter][1]}{out2}'
                )
                count += 2

        return conversion

    def _apply_conversion(self, fn_platelayout, conversion=None):
        if conversion is None:
            conversion = self._get_conversion()

        sample_position = list()

        with open(fn_platelayout) as f:
            reader = csv.reader(f, delimiter=',')
            for line in reader:
                plate_pos, sampleID, *_ = line
                if sampleID == '':
                    continue
                if plate_pos in conversion:
                    other_pos = conversion[plate_pos]
                    for pos in other_pos:
                        sample_position.append((pos, sampleID))
        return sample_position

    def _get_parsed_ixo_data(self, parsed_ixo_file):
        df = pd.read_csv(parsed_ixo_file)
        df = df.drop('IsIncluded Pos Call Score PeakWtFluor PeakMutFluor ManualCall WarnDesc'.split(), axis=1)
        return df

    def _get_qpcr_results_dataframe(self):
        dfp = pd.DataFrame(self.sample_position, columns='platePos sampleID'.split())
        dflist = list()
        for fn in self.parsed_ixo_files:
            _ = self._get_parsed_ixo_data(fn)
            _df = dfp.merge(_, on='platePos')
            dflist.append(_df)
        df = pd.concat(dflist)
        final = df.pivot(index='sampleID', columns='subsetName', values='UserCall')
        return final

    def _get_nocalls(self):
        no_calls = list()
        for rsid in self.df:
            for val in list(self.df[rsid].values):
                if val != 'WT' and val != 'HET' and val != 'HOM':
                    if not rsid in no_calls:
                        no_calls.append(rsid)
        return no_calls

    def _count_df_values(self, df, val):
        try:
            val_count = df.value_counts()[val]
        except KeyError:
            val_count = 0
        return val_count

    def write_per_sample_file(self):
        df = self.df.transpose().sort_index()
        for sample in df:
            df[sample].to_csv(NSXDIR / f'{sample}.qpcrsnpcheck', header=False)
        return


if __name__ == '__main__':
    import os
    import csv
    import glob
    import argparse
    import subprocess
    import pandas as pd
    
    script_dir = os.path.dirname(os.path.realpath(__file__))
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--directory", type=str, required=True,
                        help="Directory with IXO en PlateLayout files"
                        )
    args = parser.parse_args()
    location = args.directory
    Rparse_ixo = f'{script_dir}/iXOreader.R'

    user_parsed_ixo_fns = glob.glob(f'{location}/RUN*.ixo')
    plate_layout_fn = glob.glob(f'{location}/plate-layout*.xls')

    assert len(user_parsed_ixo_fns) == 3, f'{user_parsed_ixo_fns} shoud be 3 files' 
    assert len(plate_layout_fn) == 1, f'{plate_layout_fn} shoud be 1 file' 
    
    R_parse_command = f"Rscript --vanilla {Rparse_ixo} {' '.join(user_parsed_ixo_fns)}"
   
    subprocess.call(R_parse_command, shell=True)

    R_parsed_ixo_fns = glob.glob(f'{location}/RUN_*.csv')
    
    S = qSNPcheck(R_parsed_ixo_fns, plate_layout_fn[0])
    S.write_per_sample_file()
            
        
