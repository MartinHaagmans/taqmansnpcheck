import subprocess
import logging
from pathlib import Path

logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s', 
    filename='taqmanCopy.log',
    level=logging.INFO
    )

PARSER = Path("/data/dnadiag/taqmansnpcheck/qSNPcheckv0.2.py")
SNP_DIR = Path("/mnt/snpkg/")
ARCHIVE = SNP_DIR / "parsed_mh/"
DIRS = SNP_DIR.glob('*-*-*')

def check_if_dir(dir_to_check):
    return Path(dir_to_check).is_dir()

def run_command(cmd):
    try:
        subprocess.run(cmd, check=True)
    except subprocess.CalledProcessError:
        logging.error(f"ಠ_ಠ Error in command: {cmd}")
        exit()
    else:
        logging.info(f"ʘ‿ʘ Command {cmd} executed successfully")
    return

def check_if_all_files_present(dir_to_check):
    ixo_counter = 0
    plate_layout = False
    for fn in Path(dir_to_check).glob('*'):
        fn = Path(fn).name
        if fn.startswith('RUN_') and fn.endswith('.ixo'):
            ixo_counter +=1
        elif fn.startswith('plate-layout') and fn.endswith('.xls'):
            plate_layout = True
    if plate_layout and ixo_counter == 3:
        all_files_present = True
    else:
        all_files_present = False
    return all_files_present

doneSomething = False

for DIR in DIRS:
    for SUBDIR in DIR.glob('*'):
        # If SUBDIR is not a directory --> just 1 plate
        if not check_if_dir(SUBDIR) and check_if_all_files_present(DIR):
            doneSomething = True
            run_command(["python3", f"{PARSER}", "-d", f"{DIR}"])
            run_command(["mv", f"{DIR}", f"{ARCHIVE}"])
            break # No need to iterate
        
        # If SUBDIR is a directory --> multiple plates
        elif Path(SUBDIR).is_dir()and check_if_all_files_present(SUBDIR):
            doneSomething = True
            dirname = Path(SUBDIR).parent
            run_command(["python3", f"{PARSER}", "-d", f"{SUBDIR}"])
            run_command(["mkdir", "-p" , f"{ARCHIVE}/{dirname}"])
            run_command(["mv", f"{SUBDIR}", f"{ARCHIVE}/{dirname}"])
            run_command(["mv", f"{SUBDIR}", f"{ARCHIVE}/{dirname}"])
            # Remove main dir if empty
            run_command(f"if [ -z \"$( ls -A '{DIR}')\" ]; then rmdir {DIR} fi")

if not doneSomething:
    logging.info("ʕ •`ᴥ•´ʔ I woke up for this?")
elif doneSomething:
    logging.info("ʕᵔᴥᵔʔ I did something!")             